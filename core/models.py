from django.db import models

# Create your models here.


def upload_directory_path(instance, filename):
    """
    path to upload the files
    """
    print(filename)
    # file will be uploaded to MEDIA_ROOT/uploads/<filename>
    return f'uploads/{filename}'


class FileUpoad(models.Model):
    """
    model to hold the uploaded files
    * pending -->
    directory struct
    """
    file = models.FileField(upload_to=upload_directory_path, blank=False)
    dir  = models.TextField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)


    class Meta:
        verbose_name = "file_upload"
        verbose_name_plural = "file_uploads"

    def __str__(self):
        return str(self.file)
        



class GlobalBoolConf(models.Model):
    """
    model to hold the global variables (generic)
    """
    title = models.CharField(max_length=100, blank=True, null=True)
    value = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_updated = models.DateTimeField(auto_now=True,null=True)
    def __str__(self):
        return self.title



class UploadFolder(models.Model):
    """
    Model to hold the folders saved in the server,
    which bneed to be dynamically created using the upload api
    * incomplete
    """
    title = models.CharField(max_length=100, blank=True,null=True)
    folder_path = models.TextField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title




def zip_directory_path(instance, filename):
    """
    folder path to save the compressed files
    """
    print(filename)
    # file will be uploaded to MEDIA_ROOT/uploads/<filename>
    return f'zips/{filename}'



class CompressedFiles(models.Model):
    """
    Optional model to save the compressed files, perma or temp
    """
    file = models.FileField(upload_to=zip_directory_path, blank=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    def __str__(self):
        return str(self.id)
