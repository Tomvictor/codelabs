import os, sys
from django.conf import settings
from core.models import *


def check_or_create_directory(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == 17:
            # Dir already exists. No biggie.
            pass






class CoreMixin(object):

    def __init__(self,**kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


    def get_user_role(self):
        """
        # 1 admin      - super_user
        # 2 client     - staff permission
        # 3 sub-client - active only
        """
        priority = 0
        name  = ""
        if self.user.is_superuser:
            print("Admin")
            priority = 1
            name = "Admin"
        elif self.user.is_staff:
            print("Client")
            priority = 2
            name = "Client"
        else:
            print("Sub Clien")
            priority = 3
            name = "Sub Client"
        
        resp = {
            "priority" : priority,
            "name"     : name
        }
        return resp





from zipfile import ZipFile 
import os 
  
def get_all_file_paths(directory): 
  
    # initializing empty file paths list 
    file_paths = [] 
  
    # crawling through directory and subdirectories 
    for root, directories, files in os.walk(directory): 
        for filename in files: 
            # join the two strings in order to form the full filepath. 
            filepath = os.path.join(root, filename) 
            file_paths.append(filepath) 
  
    # returning all file paths 
    return file_paths         
  
def zip(directory): 
  
    # calling function to get all file paths in the directory 
    file_paths = get_all_file_paths(directory) 
  
    # printing the list of all files to be zipped 
    print('Following files will be zipped:') 
    for file_name in file_paths: 
        print(file_name) 

    
    zip_working_path = f"{settings.MEDIA_ROOT}/zips/test.zip" # hard coded
    # zip_production_path = f"{storage_path}/{instance.id}_{i}.png"
    zip_production_path = f"zips/test.zip" # hard coded


    # writing files to a zipfile 
    with ZipFile(zip_working_path,'w') as zip: 
        # writing each file one by one 
        for file in file_paths: 
            zip.write(file) 

    
    compressed_file_obj = CompressedFiles(file=zip_production_path)
    compressed_file_obj.save()
    print('All files zipped successfully!')        
    return compressed_file_obj.id 
  
