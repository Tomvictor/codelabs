from django.urls import include, path, re_path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from .views import *


app_name = 'Core'


urlpatterns = [
    path('upload/', FileUpload.as_view(), name="compress-api"),
    path('compress/', CompressAPIView.as_view(), name="compress-api"),
    path('folder-list/', FolderListApiView.as_view(), name="compress-api"),
    path('login/', LoginAPIView.as_view(), name="login-api"),
    path('api-token-auth/', obtain_jwt_token),
    path('api-token-refresh/', refresh_jwt_token)
]

