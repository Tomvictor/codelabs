from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth import get_user_model
from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,

)
from rest_framework import status
from django.http import JsonResponse
from rest_framework.generics import *
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token

from datetime import datetime, timedelta

from django.contrib.auth import authenticate, login

from rest_framework_jwt.utils import jwt_payload_handler, jwt_encode_handler

from rest_framework.parsers import MultiPartParser, FormParser, JSONParser


from rest_framework import mixins, views
from rest_framework.generics import GenericAPIView
from .trans import *

from .models import *


from django.utils.encoding import smart_str



from django.http import HttpResponse


# Create your views here.
User = get_user_model()



# login api
class LoginAPIView(APIView):
    """
    Request:
    {
        "username" : "tom",
        "password" : "asd123##"
    }
    response:
    {
    "username": "abin",
    "token": "fa37dfece8091c13804fc7b3c3a0fce231cc874a",
    "id": 5
    }
    """

    permission_classes = [AllowAny]

    def get(self,*args, **kwargs):
        return Response("GET method is not allowed",status = status.HTTP_400_BAD_REQUEST)

    def post(self,request,format=None):
        data = request.data
        status_code = "0"
        _message = "invalid"
        authKey = ""
        userId = 0

        try:
            username = data["username"]
            password = data["password"]
        except:
            print("unable to get params")
            status_code = "0"
            _message = "invalid input"
            resp = {
                "message": _message,
                "status" : status_code
            }
            context = {
                "authKey" : authKey,
                "userId" : userId,
                "message" : resp
            }
            return Response(context,status=status.HTTP_200_OK)

        try:
            print("check whether the user exists")
            this_user = get_object_or_404(User, username=username)
            # status_code = "0"

            if not this_user.is_active:
                _message = "Please complete email verification!"
                print(_message)
                status_code = "0"
                resp = {
                    "message": _message,
                    "status": status_code
                }
                context = {
                    "authKey": authKey,
                    "userId": userId,
                    "message": resp
                }
                return Response(context,status=status.HTTP_200_OK)

        except:
            status_code = "0"
            _message = "User does not exist"
            print(_message)
            resp = {
                "message": _message,
                "status": status_code
            }
            context = {
                "authKey": authKey,
                "userId": userId,
                "message": resp
            }
            return Response(context, status=status.HTTP_200_OK)

        # check whether user active or not



        user = authenticate(request, username=username, password=password)
        if user is not None:
            _message = "Authenticated"
            status_code = "1"
            print(_message)
            print(user)
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            print(token)
            print(user.id)
            user_id = user.id

            resp = {
                "message": _message,
                "status" : status_code
            }
            current_time = datetime.now()
            token_expiry_date     = current_time + settings.TOKEN_EXP_DELTA
            context = {
                "authKey" : token,
                "tokenExpirationDate" : token_expiry_date,
                "tokenExpirationTimestamp": int(token_expiry_date.replace(tzinfo=None).timestamp()),
                # "tokenExpirationTimestamp":datetime.timestamp(token_expiry_date),
                "userId" : user_id,
                "username" : user.username,
                "fullName" : user.get_full_name(),
                "message" : resp
            }
            # this_serializer = JWTSerializer(data=context)
            # if this_serializer.is_valid():
            return Response(context, status=status.HTTP_200_OK)
        else:
            _message = "invalid credentials"
            status_code = "0"
            print(_message)
            resp = {
                "message": _message,
                "status": status_code
            }
            context = {
                "message" : resp,
            }
            return Response(context,status=status.HTTP_200_OK)


# upload api
class FileUpload(APIView):
    permission_classes = [IsAuthenticated]
    parser_classes = (MultiPartParser, FormParser)

    def get(self, request, *args, **kwargs):
        _status = 0
        _message = "Testing"
        this_user = request.user
        trans = CoreMixin(user=this_user)
        user_params = trans.get_user_role()
        print("parsing files")
        file_obj      = request.FILES['file']
        print(file_obj)
        print(dir(file_obj))
        print("file parsed")
        fl = FileUpoad(file=file_obj)
        fl.save()

        try:
            print("parsing files")
            file_obj      = request.FILES['file']
            print(file_obj)
            print(dir(file_obj))
            print("file parsed")
            fl = FileUpoad(file=file_obj)
            fl.save()
        except:
            print("file parsing error")
        # fetch the name
        message = {
            "status" : _status,
            "message" : _message
        }

        body = {
            "message" : message,
            "user" : this_user.username,
            "file_name" : str(file_obj),
            "file_size" : file_obj.size,
            "user_role" : user_params["name"],
            "user_priority" : user_params["priority"]
        }
        return Response(body, status=status.HTTP_200_OK)

class FolderListApiView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        data_list = []
        for item in UploadFolder.objects.all():
            atomic = {
                "title" : item.title,
                "id"    : item.id
            }
            data_list.append(atomic)
        return Response(data_list, status=status.HTTP_200_OK)



class CompressAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        _status = 0
        _message = ""
        requested_folder_id = request.GET.get("q")
        print(requested_folder_id)

        # check the user privilages
        this_user = request.user
        trans = CoreMixin(user=this_user)
        user_params = trans.get_user_role()
        print(user_params)

        # check the one at a time condition 
        server_avilability = GlobalBoolConf.objects.get(title=settings.ACCES_LOCK)
        print(server_avilability)
        if server_avilability.value is not True:
            _message = "server not avilable"
            resp = {
                "status" : 0,
                "message" : _message
            }
            return Response(resp, status=status.HTTP_200_OK)
        
        # modify the avilability
        server_avilability.value = False
        server_avilability.save()
        # get the user priority


        # get the folder path
        
        try:
            print("retriving obj")
            folder = UploadFolder.objects.get(id=requested_folder_id)
            print(folder)
            print(folder.folder_path)
        except:
            _message = "unable to retive the requested folder"
            print(_message)
            resp = {
                "message" : _message,
                "status"  : _status
            }
            return Response(resp, status=status.HTTP_200_OK)

        # get test file and replace with zip file
        file = FileUpoad.objects.get(id=1)
        print(file.file.path)
        file_name = file.file

        # begin compression
        media_path = settings.MEDIA_ROOT
        absolute_path = media_path + folder.folder_path
        print(absolute_path)
        zip_file_id = zip(absolute_path)

        # get the compressed file
        file = CompressedFiles.objects.get(id=zip_file_id)
        print(file.file.path)
        file_name = file.file
        

        response = HttpResponse(content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(file_name)
        response['X-Sendfile'] = smart_str(file.file.path)


        server_avilability.value = True
        server_avilability.save()
        print("code 9")
        return response


# media_path = settings.MEDIA_ROOT
# absolute_path = media_path + dir_path

